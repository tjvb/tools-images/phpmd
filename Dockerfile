FROM php:8.0-alpine as installer

ARG PHPMDVERSION=*

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN mkdir -p /tmp/phpmd && \
    cd /tmp/phpmd && \
	/usr/bin/composer require phpmd/phpmd:${PHPMDVERSION}

FROM php:8.0-alpine

RUN mkdir -p /tools
COPY --from=installer /tmp/phpmd /tools/phpmd
RUN ln -s  /tools/phpmd/vendor/bin/phpmd /usr/bin/phpmd